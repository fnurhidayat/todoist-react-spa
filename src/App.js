import { useState, useEffect, Fragment } from "react";
import axios from "axios";
import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
} from "firebase/auth";
import firebase from "./services/firebaseService";
import dayjs from "dayjs"
import logo from "./logo.svg";
import "./App.css";

const BASE_URL = process.env.REACT_APP_BACKEND_URL || 'http://localhost:8000';

// Initialize auth.
const auth = getAuth(firebase);

function App() {
  const [registerEmail, setRegisterEmail] = useState("");
  const [registerPassword, setRegisterPassword] = useState("");
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");
  const [content, setContent] = useState("");
  const [tasks, setTasks] = useState([]);
  const [dueAt, setDueAt] = useState(dayjs().format("YYYY-MM-DD"));
  const [user, setUser] = useState(null);

  onAuthStateChanged(auth, (user) => {
    setUser(user);

    localStorage.setItem("ACCESS_TOKEN", JSON.stringify(user?.accessToken));
  });

  // Fungsi ini akan digunakan untuk memanggil fungsi register
  // di dalam Firebase Auth.
  function onRegisterSubmitted(e) {
    e.preventDefault();
    console.log("Registering");

    createUserWithEmailAndPassword(auth, registerEmail, registerPassword)
      .then(() => console.log("Registered!"))
      .catch(console.log);
  }

  function onLogoutClick(e) {
    e.preventDefault();

    signOut(auth)
      .then(() => console.log("Logged out!"))
      .catch(console.log);
  }

  // Fungsi ini akan digunakan untuk memanggil fungsi login
  // di dalam Firebase Auth.
  function onLoginSubmitted(e) {
    e.preventDefault();
    console.log("Logging in!");

    signInWithEmailAndPassword(auth, loginEmail, loginPassword)
      .then(console.log)
      .catch(console.log);
  }

  // Get Task From Database
  function loadTasks() {
    const token = JSON.parse(localStorage.getItem("ACCESS_TOKEN") || "");

    axios
      .get(`${BASE_URL}/api/v1/tasks`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        setTasks(response.data.data.tasks);
      })
      .catch(console.log);
  }

  function onTaskCreationSubmitted(e) {
    e.preventDefault();

    console.log("Task submitted!");
    console.log(content);
    console.log(dueAt);

    const token = JSON.parse(localStorage.getItem("ACCESS_TOKEN") || "");

    axios
      .post(
        `${BASE_URL}/api/v1/tasks`,
        {
          content,
          dueAt,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
      .then(loadTasks)
      .catch(console.log);
  }

  useEffect(() => {
    if (!user) return;

    console.log("Fetching tasks!");
    loadTasks();
  }, [user]);

  return (
    <div className="App">
      <header className="App-header">
        <h1>{user?.email}</h1>

        {!user && (
          <Fragment>
            <form onSubmit={onRegisterSubmitted}>
              <input
                type="email"
                onChange={(e) => setRegisterEmail(e.target.value)}
                value={registerEmail}
              />
              <input
                type="password"
                onChange={(e) => setRegisterPassword(e.target.value)}
                value={registerPassword}
              />
              <input type="submit" value="Register" />
            </form>

            <form onSubmit={onLoginSubmitted}>
              <input
                type="email"
                onChange={(e) => setLoginEmail(e.target.value)}
                value={loginEmail}
              />
              <input
                type="password"
                onChange={(e) => setLoginPassword(e.target.value)}
                value={loginPassword}
              />
              <input type="submit" value="Login" />
            </form>
          </Fragment>
        )}

        {!!user && (
          <Fragment>
            <button onClick={onLogoutClick}>Logout</button>

            <form onSubmit={onTaskCreationSubmitted}>
              <input
                type="text"
                onChange={(e) => setContent(e.target.value)}
                value={content}
              />
              <input
                type="date"
                onChange={(e) => setDueAt(e.target.value)}
                value={dueAt}
              />
              <input type="submit" value="Create Task" />
            </form>
          </Fragment>
        )}

        <ul>
          {tasks.map((task) => (
            <li key={task.id}>{task.content}</li>
          ))}
        </ul>
      </header>
    </div>
  );
}

export default App;
